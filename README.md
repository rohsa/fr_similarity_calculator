# Frequence Response Similarity Calculator

## Notice

This tool require a folder contains measurement data which is not public, if you just want to see which IEM/Headphones are similar check the output folder.

If you want to generate the similarity table for your data, you need to prepare a folder contains `index.json` and measurement data files, an example of `index.json` is below, you also need to modify `Databases` in `similarity_calculator.py`.

``` text
{
  "Moondrop Aria": ["Moondrop Aria L.txt"],
  "Moondrop Blessing 2": ["Moondrop Blessing 2 L.txt"]
}
```

## Usage

Initialize:

``` text
python3 -m venv ~/.venv_fr_similarity_calculator
. ~/.venv_fr_similarity_calculator/bin/activate
pip3 install -r requirements.txt
```

Run calculator:

``` text
.  ~/.venv_fr_similarity_calculator/bin/activate
python3 similarity_calculator.py
```

## License

The code itself is MIT License, but the output result should not be redistributed unless the owner of the measurement data allowed you.

© 2022 Rohsa All rights reserved. 

