import os
import json
import math
import re
import numpy as np

Databases = [
    {
        "path": "../fr_database/Banbeu IEMs",
        "type": "iem",
        "available_targets": [
            "Antdroid Target",
            "Bad Guy Target",
            "Banbeucmas Target",
            "Diffuse Field Target",
            "Etymotic Target",
            "Free Field Target",
            "Harman 2017 Target",
            "Harman 2019 Target",
            "IEF Neutral Target",
            "Innerfidelity ID Target",
            "Precogvision Target",
            "Rtings Target",
            "Sonarworks Target",
            "Super Review Target",
            "Toranku Target"
        ]
    },
    {
        "path": "../fr_database/Crinacle IEMs",
        "type": "iem",
        "available_targets": [
            {
                "name": "Harman IE 2019 v2 Target", 
                "filename": "Harman IE 2019 v2 Target.comp.txt"
            },
            "IEF Neutral Target"
        ]
    },
    {
        "path": "../fr_database/Crinacle Headphones",
        "type": "headphone",
        "available_targets": [
            "Harman AE OE 2018 Target",
            "IEF Neutral Target"
        ]
    },
    {
        "path": "../fr_database/Precog IEMs",
        "type": "iem",
        "available_targets": [
            "Precog Target",
            "Antdroid Target",
            "Banbeu Target",
            "HBB Target",
            "MRS Target",
            "Tork V5 Target",
            "Harman 2019 IEM Target",
            "IEF Neutral Target"
        ]
    },
    {
        "path": "../fr_database/SuperReview IEMs",
        "type": "iem",
        "available_targets": [
            "Super Review Target",
            "Moondrop Blessing 2 Target",
            "Antdroid Target",
            "Bad Guy Target",
            "Banbeucmas Target",
            "Crinacle Neutral Target",
            "Precogvision Target",
            "RikudouGoku Target"
        ]
    },
    {
        "path": "../fr_database/TechPowerUp IEMs",
        "type": "iem",
        "available_targets": [
            "Diffuse Field Target",
            "Etymotic Target",
            "Free Field Target",
            "Innerfidelity ID Target",
            "Antdroid Target",
            "Bad Guy Target",
            "Banbeucmas Target",
            "Crinacle Target",
            "Precogvision Target",
            "Super Review Target",
            "VSG Target",
            "Harman 2019 In-Ear Target",
            "Rtings Target",
            "Sonarworks Target"
        ]
    }
]
TargetDirectory = "targets"
OutputDirectory = "public"
IndexFilename = "index.json"
FrequenceRange = (20, 20000)
NormalizeDB = 60
NormalizeHz = 500
SplitFRRowRegex = re.compile("[\s,;]+")
# the distance weights are adjusted manually, it's tricky but I don't have a better solution...
DistanceWeights = [
    (20,    0.5),
    (1000,  1.0),
    (6000,  0.5),
    (10000, 0.25)
]
# ignore difference below threshold
DistanceThreshold = 0.5

def octave_bands(fraction):
    bands = [FrequenceRange[0]]
    steps = 2 ** (1/fraction)
    while bands[-1] < FrequenceRange[1]:
        value = round(bands[-1] * steps * 10) / 10
        if value >= 1000:
            value = int(value) // 10 * 10
        elif value >= 100:
            value = int(value)
        bands.append(value)
    bands[-1] = FrequenceRange[1]
    return bands

class SimilarityCalculator(object):
    def __init__(self, database_info, fraction = 48, top_k = 10):
        self.database_info = database_info
        self.dir_path = database_info["path"]
        self.type = database_info["type"]
        self.available_targets = database_info.get("available_targets") or []
        self.fraction = fraction
        self.top_k = top_k
        self.index = json.load(open(os.path.join(self.dir_path, IndexFilename), "r"))
        self.octave_bands = octave_bands(fraction)
        self.normalize_index = next(
            i for i, f in enumerate(self.octave_bands) if f >= NormalizeHz)
        self.distance_weight = np.array([
            self._calc_distance_weight(f) for f in self.octave_bands ])

    @staticmethod
    def _calc_distance_weight(frequence):
        for f, w in reversed(DistanceWeights):
            if frequence > f:
                return w
        return 0

    def _load_fr(self, path):
        xs = []
        ys = []
        # the measurement data is NOT csv, we can't use pandas here
        # it contains junks like "source    Headset Mic 1 Low Range"
        with open(path, "r", errors="replace") as f:
            for line in f:
                if not line or line[0] == "*":
                    continue
                cells = SplitFRRowRegex.split(line)
                if len(cells) >= 2:
                    try:
                        x = float(cells[0]) # Frequence
                        y = float(cells[1]) # SPL
                        if not (math.isnan(x) or math.isnan(y)):
                            xs.append(x)
                            ys.append(y)
                    except ValueError:
                        continue
        if not xs:
            raise RuntimeError(f"Load file error: {path}")
        np_xs = np.array(xs)
        np_ys = np.array(ys)
        # resample
        sample = np.interp(self.octave_bands, np_xs, np_ys)
        # normalize
        sample -= sample[self.normalize_index] - NormalizeDB
        return sample

    @staticmethod
    def _filename_to_param(filename):
        match = re.match("^(.+) \S{1,2}\..+$", filename)
        param = match[1] if match else os.path.splitext(filename)[0]
        return param.replace(" ", "_")

    def _find_similar(self, sample, np_samples):
        distances_vec = np.maximum(np.abs(
            sample - np_samples) - DistanceThreshold, 0) * self.distance_weight
        distances = np.mean(distances_vec, axis=1)
        top_partition = np.argpartition(distances, self.top_k+1)[:self.top_k+1]
        top_indices = sorted(top_partition.tolist(), key = lambda i: distances[i])
        top_distances = [ round(distances[i], 5) for i in top_indices ]
        return top_indices, top_distances

    def calc(self):
        entries = [(k, v[0]) for k, v in self.index.items()]
        samples = []
        result = []
        for _, filename in entries:
            sample = self._load_fr(os.path.join(self.dir_path, filename))
            samples.append(sample)
        np_samples = np.array(samples, dtype=np.float32)
        for index, ((name, filename), sample) in enumerate(zip(entries, samples)):
            top_indices, top_distances = self._find_similar(sample, np_samples)
            similar_list = [
                (sn, d)
                for i, d in zip(top_indices, top_distances)
                if (sn := entries[i][0]) != name
            ]
            result.append({
                "name": name,
                "param": self._filename_to_param(filename),
                "similar_to": [ sn for sn, _ in similar_list ],
                "distances": [ d for _, d in similar_list ]
            })
        for target in self.available_targets:
            if isinstance(target, dict):
                name = target["name"]
                filename = target["filename"]
            else:
                name = target
                filename = f"{target}.txt"
            sample = self._load_fr(os.path.join(
                TargetDirectory, self.type, filename))
            top_indices, top_distances = self._find_similar(sample, np_samples)
            similar_list = [
                (entries[i][0], d)
                for i, d in zip(top_indices, top_distances)
            ][:self.top_k]
            result.append({
                "name": name,
                "param": self._filename_to_param(name),
                "is_target": True,
                "similar_to": [ sn for sn, _ in similar_list ],
                "distances": [ d for _, d in similar_list ],
            })
        return result

def main():
    for database_info in Databases:
        dir_path = database_info["path"]
        print("Calculating", dir_path)
        calculator = SimilarityCalculator(database_info)
        result = calculator.calc()
        if not os.path.isdir(OutputDirectory):
            os.makedirs(OutputDirectory)
        output_path = os.path.join(
            OutputDirectory, os.path.basename(dir_path) + ".json")
        with open(output_path, "w") as f:
            json.dump(result, f, indent=2)
        print("Similarity Table Saved")
    print("Done")

if __name__ == "__main__":
    main()

